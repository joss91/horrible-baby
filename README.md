# Crying Baby

Développer un jeu où il faut intéragir avec un bébé qui pleure
Style animal virtuel (tamagotchi)

## Consignes

- le bébé peut être dans deux états, soit il est content (et il n'y a rien à faire), soit il pleure et il faut intervenir (en fonction de pourquoi il pleure).

- le bébé pleure pour 3 raisons: il faut le changer, il faut le nourire, ou alors il pleure pour rien (c'est un bébé)...

- il faut prévoir plusieurs boutons pour intéragir avec le bébé: donner à manger, changer et jouer avec (s'il pleure pour rien).

- Par défaut, le bébé est dans son état "content", au bout de 10 secondes, un état doit etre choisi au hasard, soit il pleure (pour une raison ou pour une autre), soit il reste content

- S'il est dans un état de pleur, il ne changera pas d'état avant qu'il y ai eu intervention (la bonne intervention)

- Si jamais il est laissé dans son état de pleure pendant plus d'une minute, alors c'est perdu (les services sociaux emportent le bébé #oliverTwist)

- Dès qu'il change d'état, un son doit retentire (s'il se met à pleurer, un son de pleur, s'il est content, un son de joie)

__OPTIONS__

- Mettre en forme l'interface du jeu, en CSS (voir en responsive, avec framework CSS ou non)

- Désormais il y a 3 états du bébé, content, pleure et pas content, quand il est pas content, il faut jouer avec pour qu'il soit de nouveau content (et ducoup il ne pleure plus pour rien)

- Quand il fait la tête et qu'on joue avec lui, alors il affiche sa tête avec la langue tirée

- Quand on perd, le bébé affiche sa face "surpris"

- Ajouter une transition en CSS pendant le changement d'état du bébé (et donc d'image) 

- Ajouter un écran Game Over et / ou changer la couleur de fond (avec transition CSS)


## Ressources

Les resources sont fournis, les sons et vignettes pour représenter l'état deu bébé.

Les images pour informer de la raison pour laquelle de bébé pleure

Les fichiers finaux à utiliser sont dans le dossier ./src/assets/
